<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://http://recruitment.kszczygiel9.pl/
 * @since      1.0.0
 *
 * @package    Coding_Exercise
 * @subpackage Coding_Exercise/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Coding_Exercise
 * @subpackage Coding_Exercise/includes
 * @author     Krzysztof Szczygieł <krzysiek.szczygiel96@gmail.com>
 */
class Coding_Exercise_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'coding-exercise',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
