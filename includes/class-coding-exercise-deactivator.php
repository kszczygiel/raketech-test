<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://http://recruitment.kszczygiel9.pl/
 * @since      1.0.0
 *
 * @package    Coding_Exercise
 * @subpackage Coding_Exercise/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Coding_Exercise
 * @subpackage Coding_Exercise/includes
 * @author     Krzysztof Szczygieł <krzysiek.szczygiel96@gmail.com>
 */
class Coding_Exercise_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
