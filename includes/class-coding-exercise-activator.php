<?php

/**
 * Fired during plugin activation
 *
 * @link       https://http://recruitment.kszczygiel9.pl/
 * @since      1.0.0
 *
 * @package    Coding_Exercise
 * @subpackage Coding_Exercise/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Coding_Exercise
 * @subpackage Coding_Exercise/includes
 * @author     Krzysztof Szczygieł <krzysiek.szczygiel96@gmail.com>
 */
class Coding_Exercise_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
