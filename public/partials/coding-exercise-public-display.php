<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://http://recruitment.kszczygiel9.pl/
 * @since      1.0.0
 *
 * @package    Coding_Exercise
 * @subpackage Coding_Exercise/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
