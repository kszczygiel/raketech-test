<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://http://recruitment.kszczygiel9.pl/
 * @since      1.0.0
 *
 * @package    Coding_Exercise
 * @subpackage Coding_Exercise/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
