<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://http://recruitment.kszczygiel9.pl/
 * @since             1.0.0
 * @package           Coding_Exercise
 *
 * @wordpress-plugin
 * Plugin Name:       Coding Exercise
 * Plugin URI:        https://https://gitlab.com/kszczygiel/raketech-test
 * Description:       This is a description of the plugin.
 * Version:           1.0.0
 * Author:            Krzysztof Szczygieł
 * Author URI:        https://http://recruitment.kszczygiel9.pl/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       coding-exercise
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CODING_EXERCISE_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-coding-exercise-activator.php
 */
function activate_coding_exercise() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-coding-exercise-activator.php';
	Coding_Exercise_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-coding-exercise-deactivator.php
 */
function deactivate_coding_exercise() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-coding-exercise-deactivator.php';
	Coding_Exercise_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_coding_exercise' );
register_deactivation_hook( __FILE__, 'deactivate_coding_exercise' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-coding-exercise.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_coding_exercise() {

	$plugin = new Coding_Exercise();
	$plugin->run();

}
run_coding_exercise();
